# Topic Modeling Course

This repository contains some basic code for the topic modeling course that I
taught at the Vienna Models & Methods summer school, July, 2022.

## LICENSE

All code here is licensed under the MIT License. See the LICENSE file.
